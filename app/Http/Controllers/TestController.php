<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Controller for testing role middleware
class TestController extends Controller
{
    
     public function index(){
      echo "<br>Test Controller.";
   }
}
