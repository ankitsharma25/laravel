<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$request->session()->put(['abcd'=>'student']);
        //return $request->session()->get('abcd');


       // session(['abcd'=>'teacher']); #global function
       // return session('abcd');

       // $request->session()->forget('abcd'); #deleting sessions
       //  return session('abcd');

        //Flash
        //$request->session()->flash('messsage','Your post is created');
//        $request->session()->reflash();
//        $request->session()->keep('message');


        return $request->session()->get('messsage');

       // return view('home');
    }
}
