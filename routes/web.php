<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    if(Auth::check()){
        return "The user is logged in";
    }else{
        return view('welcome');
    }

});

//sample route
//redirecting to a specific rout named profile
Route::get('/foo', function () {
    return redirect()->route('profile');
});

//geting parameters from url
//we can use {abc?} to set default parameters
Route::get('/bar/{id}/{name}/{class?}',function($id,$name,$class='Computers'){
	return 'Hello '.$id.' Name '.$name .' Class '.$class;
})->where(['id' => '[0-9]+', 'name' => '[a-z]+']);


//passing parameters to middleware
Route::get('role',[
   'middleware' => 'Role:editor',
   'uses' => 'TestController@index',
]);


//naming a route by sing name()
// here user/profile route is attached to TestController with the method index
Route::get('user/profile', 'TestController@index')->name('profile');



Auth::routes();

Route::get('/home', 'HomeController@index');


Route::get('/admin/user/roles',['middleware'=>['Role','auth','web'],function (){
    return "Middleware role";
}]);


Route::get('/admin','AdminController@index');


Route::get('/email',function (){
    $data = [
        'title' => 'Some title',
        'Content'=> 'This is some content '
    ];
    //set the test view in the
    Mail::send('emails.test',$data,function (){
        $message->to('somemailid@email.com','Recipent Name')->subject('Hi this is a subject');
    });
});